﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeuralNet
{
    public partial class Form1 : Form
    {
        int count = 49999;//first 50k are reserved for training
        int iAccuracy = 0;
        int intervalSpeed = 700; //speed to change images.
        int numOfEpochs = 1; //number of epochs to run when training.

        NeuralNetwork nn = new NeuralNetwork();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // MNISTRead mnistTrain = new MNISTRead();


        }

        //transforms bytearrayIn into Image
        public Image byteArrayToImage(byte[] byteArrayIn)
        {
            int index = 0;
            Bitmap bmp = new Bitmap(28,28);

            for (int i = 0; i < bmp.Height ; i++)
            {
                int gray = 40;
                int background = 105;
                for (int j = 0; j < bmp.Width; j++, index++)
                {
                    Color c = Color.FromArgb(byteArrayIn[index], byteArrayIn[index], byteArrayIn[index]);
                    //Debug.WriteLine("Hello");
                    if (c.R > 100)
                    {
                        c = Color.FromArgb(gray, gray, gray);
                    }
                    else if (c.R < 50)
                    {
                        c = Color.FromArgb(background, background, background);
                    }
                    bmp.SetPixel(j, i, c);
                }
            }

            return (Image)bmp;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
                       
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            UpdateInputs();            
        }

        //updates the image boxes and labels to the appropiate info
        private void UpdateInputs()
        {
            //check bound
            if (count < nn.mnistTrain.labels.Length)
            {
                pictureBox1.Image = byteArrayToImage(nn.mnistTrain.images[count]);
                label3.Text = "" + nn.mnistTrain.labels[count];
                nn.SetInputs(nn.mnistTrain.images[count], count);
                nn.PropagateForward();
                label4.Text = "" + nn.GetOutput();

                if (nn.mnistTrain.labels[count] == nn.GetOutput())
                {
                    iAccuracy++;
                }
                label6.Text = $"{iAccuracy}/{count - 49998}";

                count++;
            }

            //accelerate timer for fun
            if (intervalSpeed != 1)
            {
                intervalSpeed--;
                timer1.Interval = intervalSpeed;
            }

            //Update correct guess rate
        }
        public void InitTimer()
        {
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = intervalSpeed; // in miliseconds
            timer1.Start();
        }

        private void trainToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Console.WriteLine($"Training... ");
            var watch = System.Diagnostics.Stopwatch.StartNew();
            
            for (int i = 0; i < numOfEpochs; i++)
            {
                nn.TrainNN();
                Console.WriteLine($"{i+1}/{numOfEpochs}");
            }
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            Console.WriteLine($"Done Training! Time Elapsed: {elapsedMs/1000} seconds");

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            InitTimer();
        }
    }
}
