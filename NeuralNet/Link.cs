﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNet
{
    //The connection between a node from one layer to the next. Holds Weight that is useful when propagating.
    class Link
    {
        public Node from;
        public Node to;
        double weight;
        double deltaWeight = 0; //starts at 0 because it gets added onto, not set.

        public Link(Node from, Node to, double weight)
        {
            from.AddfConnection(this);
            this.from = from;

            to.AddbConnection(this);
            this.to = to;

            this.weight = weight;

            //Console.WriteLine($"Weight is {weight}");
        }

        public void SetWeight(double weight)
        {
            this.weight = weight;
        }

        public double GetWeight()
        {
            return  weight;
        }

        //sets deltaWeight to bSlope * activation of the from node * -learnRate
        public void SetDeltaWeight(double bSlope, double learnRate)
        {
            deltaWeight = (bSlope * from.value) * (learnRate * -1);
        }

        //This is the same as weight * activation of the fromNode
        public double WeightedValue
        {
            //Console.WriteLine($" {from.value} * {weight} ... totals {from.value * this.weight}")
            get {return (from.value * this.weight); }
        }

        //Sets the new Weight, equal to the average sum of of the weights
        public void SetNewWeight(int repetitions)
        {
            weight = (deltaWeight / repetitions) + weight;

            deltaWeight = 0;
        }

    }
}
