﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNet
{
    //Holds all layers and methods that make a Neural Net.
    class NeuralNetwork
    {
        Layer inputLayer;
        Layer hiddenLayer;
        Layer hiddenLayer2;
        Layer outputLayer;
        double learnRate = 3.0;
        int deltaRepetitions = 10; //number of times to repeat before applying new weights and bias
        const int IMAGE_SIZE = 784;
        const int OUTPUT_SIZE = 10;

        Random rnd = new Random();
        
        public MNISTRead mnistTrain = new MNISTRead();
       
        int index = 0;

        //Encharged of creating all Layers, setting inputs, connecting the layers together, forwardFeed and backPropagation
        public NeuralNetwork()
        {            
            //The hidden layer sizes can be changed to any int.
            //TODO: make inputLayer, outputlayer and hiddenLayer subclass
            inputLayer = new Layer(IMAGE_SIZE, rnd);
            hiddenLayer = new Layer(16, rnd);
            hiddenLayer2 = new Layer(16, rnd);
            outputLayer = new Layer(OUTPUT_SIZE, rnd);

            new Random().ShuffleData(mnistTrain.images, mnistTrain.labels);

            SetInputs(mnistTrain.images[index], index);

            ConnectAdjacentLayers(inputLayer, hiddenLayer);
            ConnectAdjacentLayers(hiddenLayer, hiddenLayer2);
            ConnectAdjacentLayers(hiddenLayer2, outputLayer);            
        }


        //dummy Neural Net. This net has hardcoded values and should be used only when testing values.
        public NeuralNetwork(int dummy)
        {

            //TODO: make inputLayer, outputlayer and hiddenLayer subclass
            inputLayer = new Layer(4, rnd);
            hiddenLayer = new Layer(3, rnd);
            //no hidden2
            outputLayer = new Layer(2, rnd);

            inputLayer.nodes[0].value = 0.1;
            inputLayer.nodes[1].value = 0.3;
            inputLayer.nodes[2].value = 0.5;
            inputLayer.nodes[3].value = 0.5;

            ConnectAdjacentLayers(inputLayer, hiddenLayer);
            ConnectAdjacentLayers(hiddenLayer, outputLayer);

            for (int i = 0; i < inputLayer.nodes.Length; i++)
            {
                inputLayer.nodes[i].value = (((double)i / 10));
                foreach (var link in inputLayer.nodes[i].fConnections)
                    link.SetWeight(1 + (((double)i / 10)));
            }
            for (int i = 0; i < hiddenLayer.nodes.Length; i++)
            {
                hiddenLayer.nodes[i].bias = 0.3 + (((double)i / 10));
                foreach (var link in hiddenLayer.nodes[i].fConnections)
                    link.SetWeight(1 + (((double)i / 20)));
            }
            for (int i = 0; i < outputLayer.nodes.Length; i++)
            {
                outputLayer.nodes[i].bias = 1 + (((double)i / 5));
            }

            PropagateForward();

            outputLayer.nodes[1].y = 1;

            PropagateBackward(outputLayer);
            PropagateBackward(hiddenLayer2);
            //
        }

        //Sets the inputs of the inputLayer nodes equal to the byteArrayIn.
        // index is the current index of the training data.
        public void SetInputs(byte[] byteArrayIn, int index)
        {
            this.index = index;
            for (int i = 0; i < inputLayer.nodes.Length; i++)
            {
                inputLayer.nodes[i].value = byteArrayIn[i]/255.0;
            }
            //inputLayer.nodes[0].value = 114.0 / 255.0;
            //inputLayer.nodes[1].value = 63.0 / 255.0;
        }

        //Returns the output of the neural net, and sets the ys of the outputLayer nodes
        public int GetOutput()
        {
            double max = outputLayer.nodes[0].value;
            int maxOutput = 0;
            //Console.WriteLine($"Node {0} has a Value of {outputLayer.nodes[0].value}");
            //foreach (var link in outputLayer.nodes[0].bConnections)
            for (int i = 1; i < outputLayer.nodes.Length; i++)
            {   
                //Console.WriteLine($"Vale of node {i}'s bias {outputLayer.nodes[i].bias}");
                //Console.WriteLine($"Node {i} has a Value of {outputLayer.nodes[i].value}");
                if (outputLayer.nodes[i].value > max)
                {
                    max = outputLayer.nodes[i].value;
                    maxOutput = i;
                }
            }
            return maxOutput;
        }

        //sets the y's of the outputLayer Nodes
        public void SetOuputYs()
        {
            //set all ys to 0.
            for (int i = 0; i < outputLayer.nodes.Length; i++)
            {
                outputLayer.nodes[i].y = 0;
            }

            //set y to 1 to the expected output
            int k = mnistTrain.labels[index];
            outputLayer.nodes[k].y = 1;
            
            /*for (int i = 0; i < outputLayer.nodes.Length; i++)
            {
                Console.WriteLine($"Node {i} has a y of {outputLayer.nodes[i].y}");
            }
            Console.WriteLine($"------------------------------------------------------");*/
        }

        //Calculates the activation of all the nodes in layer.
        public Layer FeedLayer(Layer layer)
        {
            for (int i = 0; i < layer.nodes.Length; i++)
            {
                layer.nodes[i].FeedForward();
            }
            return layer;
        }

        //Feedss all layers in the neural net (excluding input Layer)
        public void PropagateForward()
        {
            for (int i = 0; i < hiddenLayer.nodes.Length; i++)
            {
                hiddenLayer.nodes[i].FeedForward();
                //Console.WriteLine($"Hidden {i} has a Value of {hiddenLayer.nodes[i].value}");
            }
            for (int i = 0; i < hiddenLayer2.nodes.Length; i++)
            {
                hiddenLayer2.nodes[i].FeedForward();
                //Console.WriteLine($"Hidden2 {i} has a Value of {hiddenLayer2.nodes[i].value}");
            }
            for (int i = 0; i < outputLayer.nodes.Length; i++)
            {
                outputLayer.nodes[i].FeedForward();
                //Console.WriteLine($"Output {i} has a Value of {hiddenLayer2.nodes[i].value}");
            }
        }

        //Propagetes the Layer layer backward
        public void PropagateBackward(Layer layer)
        {
            //check if it's output layer
            if (layer == outputLayer)
            {
                //calculate dC/da of every node
                for (int i = 0; i < layer.nodes.Length; i ++)
                {
                    layer.nodes[i].CalculateOutputDCda();
                    layer.nodes[i].SetDeltas(learnRate);
                }

            }
            else
            {
                for (int i = 0; i < layer.nodes.Length; i++)
                {
                    layer.nodes[i].CalculateDCda();
                    layer.nodes[i].SetDeltas(learnRate);
                }
            }

        }

        //Links the layer fromLayer to the layer toLayer
        void ConnectAdjacentLayers(Layer fromLayer, Layer toLayer)
        {
            for (int i = 0; i < toLayer.nodes.Length; i++)
            {
                for (int j = 0; j < fromLayer.nodes.Length; j++)
                {
                    new Link(fromLayer.nodes[j], toLayer.nodes[i], (rnd.NextDouble() * 2) - 1);
                }
            }
        }

        //Sets the new deltas in Layer layer
        void SetDeltas(Layer layer, int repetitions)
        {
            for (int i = 0; i < layer.nodes.Length; i++)
            {
                layer.nodes[i].SetNewBias(repetitions);
            }
            
        }

        //Trains the Neural Net
        public void TrainNN()
        {
            new Random().ShuffleData(mnistTrain.images, mnistTrain.labels);
            int totalRepetitions = 5000;//number of times to repeat the whole process. (mnistTrain.labels.Length / deltaRepetitions / 6 * 5)
            for (int j = 0; j < totalRepetitions; j++)
            {
                for (int i = 0; i < deltaRepetitions; i++)
                {
                    //Console.WriteLine($"Training... ({index+1}/50000)");
                    SetInputs(mnistTrain.images[index], index);
                    PropagateForward();

                    SetOuputYs();

                    PropagateBackward(outputLayer);
                    PropagateBackward(hiddenLayer2);
                    PropagateBackward(hiddenLayer);

                    index++;
                }
                SetDeltas(outputLayer, deltaRepetitions);
                SetDeltas(hiddenLayer2, deltaRepetitions);
                SetDeltas(hiddenLayer, deltaRepetitions);
                
                //deltas in inputLayer don't matter (and shouldn't even exist).
            }
            //reset index
            index = 0;
            double accuracy = 0;
            for (int j = (totalRepetitions * deltaRepetitions); j < (mnistTrain.labels.Length); j++)
            {
                SetInputs(mnistTrain.images[j], j);
                PropagateForward();
                if (mnistTrain.labels[j] == GetOutput())
                {
                    accuracy++;
                }
            }
            Console.WriteLine($"Accuracy of current Epoch is  ({accuracy / 100} %)");
        }

    }

    static class RandomExtensions
    {
        //Shuffles the data using random
        public static void ShuffleData<T>(this Random rng, T[][] images, T[] labels)
        {
            int n = images.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T[] temp = images[n];
                images[n] = images[k];
                images[k] = temp;
                T temp2 = labels[n];
                labels[n] = labels[k];
                labels[k] = temp2;
            }
        }
    }
}
