﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeuralNet
{
    //The neuron that holds biases and activations which are used to determine the output of a given input.
    class Node
    {
        public List<Link> bConnections = new List<Link>();
        public List<Link> fConnections = new List<Link>();
        public double bias;
        double deltaBias = 0; //Starts at 0 because it gets added onto, not set.
        public double value = 0; //aka activation
        public double z; //value before sigmoid
        public double dCda;
        public int y = 0;
        double bSlope;

        public Node(double bias)
        {
            this.bias = bias;
            //Console.WriteLine($"Bias is {bias}");
        }

        public void AddfConnection(Link connection)
        {
            fConnections.Add(connection);
        }
        public void AddbConnection(Link connection)
        {
            bConnections.Add(connection);
        }

        public void SetValue(double value)
        {
            this.value = value;
        }

        public void SetBias(double bias)
        {
            this.bias = bias;
        }

        public void SetDeltas(double learnRate)
        {   //Set DeltaBias of the node
            bSlope = dCda * SigmoidD(z);
            deltaBias += (bSlope * (learnRate * -1));

            //set the delta Weight of every adjacent weight.
            foreach (var link in bConnections)
            {
                link.SetDeltaWeight(bSlope, learnRate);
            }

        }

        //sets the value and z of the node.
        public virtual void FeedForward()
        {
            double sum = 0;
            foreach (var link in bConnections)
                sum += link.WeightedValue;
            z = (sum + bias);
            value = Sigmoid(z);
            //Console.WriteLine($"sum {sum}");
            //Console.WriteLine($"Sigmoid {value}");

        }

        //Calculates the sigmoid of any double x
        public double Sigmoid(double x)
        {
            return (1.0 / (1.0 + Math.Exp(-x)));
        }

        //Calculates the Derivative of Sigmoid
        public double SigmoidD(double x)
        {
            double var = Sigmoid(x);
            return (var * (1.0 - var));
        }

        //Calculates the derivative of Cost in relation to the activation in the output Layer.
        public void CalculateOutputDCda()
        {
            dCda = 2.0 * (value - y);
        }

        //Calculates the derivative of Cost in relation to the activation.
        public void CalculateDCda()
        {
            double sum = 0.0;

            //weight * sigmoid(z) * toDCDA
            foreach (var link in fConnections)
            {
                sum += (link.GetWeight() * (link.to.bSlope));
            }

            dCda = sum;
        }
        
        //Sets the new Bias, equal to the average sum of number of repetitions it did back Propagations
        public void SetNewBias(int repetitions)
        {
            bias = (deltaBias/repetitions) + bias;
            //Console.WriteLine($"bias is {bias}");
            foreach (var link in bConnections)
            {
                link.SetNewWeight(repetitions);
            }
            
            //reset deltaBias
            deltaBias = 0;
        }
    }


}
